require 'api_version_constraint'

Rails.application.routes.draw do
  namespace :api, defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/' do
    namespace :v1, path: '/', constraints: ApiVersionConstraint.new(version: 1, default: true) do
      resources :cars, only: %i[index show create]
      put 'cars/:plate/pay', to: 'cars#pay'#, constraints: { plate: /[A-Za-z]{3}-\d{4}/ }
      put 'cars/:plate/out', to: 'cars#out'#, constraints: { plate: /[A-Za-z]{3}-\d{4}/ }
      resources :tickets, only: %i[index show]
      resources :interactions, only: :show
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
