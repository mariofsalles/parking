require 'rails_helper'

RSpec.describe Car, type: :model do
  let(:car) { build(:car) }
  let(:ticket) { build(:ticket) }
  let(:car_invalid) { build(:car, plate: 'Aa1-125A') }

  it { should be_mongoid_document }
  it { should have_timestamps.for(:creating) }
  it { should have_timestamps.for(:updating) }
  it { should embed_one(:ticket) }
  it { should embed_many(:interactions) }

  it { should respond_to(:name) }
  it { should respond_to(:model) }
  it { should respond_to(:year) }
  it { should have_field(:name).of_type(String) }
  it { should have_field(:model).of_type(String) }
  it { should have_field(:year).of_type(String) }
  it { should validate_presence_of(:plate) }
  it { should allow_value(car.plate).for(:plate) }
  it { should allow_value(car.plate.downcase).for(:plate) }
  it { expect(car_invalid).to_not be_valid }
end
