require 'rails_helper'

RSpec.describe Interaction, type: :model do
  let(:interaction) { build(:interaction) }

  it { should be_mongoid_document }
  it { should have_timestamps.for(:creating) }
  it { should have_timestamps.for(:updating) }

  it { should be_embedded_in(:car).as_inverse_of(:interactions) }
  it { expect(interaction).to be_embedded_in(:car) }

  it { should have_field(:inside).of_type(Mongoid::Boolean) }
  it { should have_field(:left).of_type(Mongoid::Boolean) }
  it { should have_field(:paid).of_type(Mongoid::Boolean) }
  it { should have_field(:time).of_type(String) }
  it { should have_field(:ticket_id).of_type(Ticket) }

  it { expect(interaction).to be_left }
  it { expect(interaction).to be_paid }
  it { expect(interaction).not_to be_inside }
  it { expect(interaction.time).not_to be_nil }

  it { should respond_to(:inside) }
  it { should respond_to(:left) }
  it { should respond_to(:paid) }
  it { should respond_to(:time) }
  it { should respond_to(:ticket_id) }
end
