require 'rails_helper'

RSpec.describe Ticket, type: :model do
  let(:ticket) { build(:ticket) }

  it { should be_mongoid_document }
  it { should have_timestamps.for(:creating) }
  it { should have_timestamps.for(:updating) }

  it { should be_embedded_in(:car).as_inverse_of(:ticket) }
  it { expect(ticket).to be_embedded_in(:car) }
  
  it { should have_field(:inside).of_type(Mongoid::Boolean).with_default_value_of(true) }
  it { should have_field(:left).of_type(Mongoid::Boolean).with_default_value_of(false) }
  it { should have_field(:paid).of_type(Mongoid::Boolean).with_default_value_of(false) }

  it { expect(ticket).not_to be_left }
  it { expect(ticket).not_to be_paid }
  it { expect(ticket).to be_inside }

  it { should respond_to(:inside) }
  it { should respond_to(:left) }
  it { should respond_to(:paid) }
end
