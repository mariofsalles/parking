require 'ticket'
require 'interaction'

FactoryBot.define do
  factory :car do
    name { 'Honda' }
    model { Faker::Vehicle.model(make_of_model: 'Honda') }
    year { rand(2011..2022) }
    plate { Faker::Vehicle.license_plate }
    ticket { build(:ticket) }
    interactions { [] }
  end
end
