FactoryBot.define do
  factory :interaction do
    inside { false }
    left { true }
    paid { true }
    time do
      "#{TimeDifference.between(
        Faker::Time.between_dates(from: Time.new - 1, to: Time.new, period: :morning),
        Time.new
      ).in_minutes} minutes"
    end
    ticket_id {}
  end
end
