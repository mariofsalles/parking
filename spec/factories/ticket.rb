FactoryBot.define do
  factory :ticket do
    inside { true }
    left { false }
    paid { false }
  end
end
