require 'rails_helper'

RSpec.describe 'Cars API', type: :request do
  before { host! 'api.parafuzo.test' }
  let(:headers) do
    {
      'Content-Type': Mime[:json].to_s,
      'Accept': 'application/vnd.parking.v1'
    }
  end

  describe 'GET /cars' do
    let(:car) { attributes_for(:car) }
    before { post '/cars', params: { car: car }.to_json, headers: headers }
    it 'returns status code 200' do
      get '/cars', params: {}, headers: headers
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /cars/:plate' do
    let(:car) { attributes_for(:car) }
    before { post '/cars', params: { car: car }.to_json, headers: headers }
    it 'returns status code 200' do
      get "/cars/#{car['plate']}", params: {}, headers: headers
      expect(response).to have_http_status(200)
    end
    it 'return json with car plate' do
      get "/cars/#{car['plate']}", params: {}, headers: headers
      expect(json_body.last['plate']).to eq(car[:plate])
    end
  end

  describe 'POST /cars valids' do
    let(:car_params) { attributes_for(:car) }
    before { post '/cars', params: { car: car_params }.to_json, headers: headers }
    context 'when I call API cars with valid params' do
      it 'returns the json with plate saved' do
        car_params = build(:car)
        post '/cars', params: { car: car_params }.to_json, headers: headers
        expect(json_body['reservation_number']).not_to be_nil
      end
      it 'returns status code ok for a new reservation' do
        expect(response).to have_http_status(200)
      end
      it 'is saving car with reserved ticket in database' do
        expect(Car.find_by(plate: car_params[:plate]).ticket['deleted_at']).to be_nil
      end
    end
  end

  describe 'POST /cars invalids' do
    # before { post '/cars', params: { car: car_params }.to_json, headers: headers }
    context 'when I call API cars with not permitted params' do
      it 'return error_fill_out plate property' do
        car_params = attributes_for(:car, plate: nil)
        post '/cars', params: { car: car_params }.to_json, headers: headers
        expect(response).to have_http_status(400)
        expect(json_body['error_fill_out']).not_to be_nil
      end

      it 'format plate not match with [A-Za-z]{3}-\d{4}' do
        car_params = attributes_for(:car, plate: 'XXx-X123')
        post '/cars', params: { car: car_params }.to_json, headers: headers
        expect(response).to have_http_status(400)
        expect(json_body['error_format_plate']).not_to be_nil
      end

      it 'plate already was saved in database' do
        car_params = attributes_for(:car)
        post '/cars', params: { car: car_params }.to_json, headers: headers
        post '/cars', params: { car: car_params }.to_json, headers: headers
        expect(response).to have_http_status(409)
        expect(json_body['error_duplicated_reserved']).not_to be_nil
      end
    end
  end
end
