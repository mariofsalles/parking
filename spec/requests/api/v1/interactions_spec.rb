require 'rails_helper'

RSpec.describe 'Interactions API', type: :request do
  before { host! 'api.parafuzo.test' }
  let!(:car) { build(:car) }
  let(:headers) do
    {
      'Content-Type': Mime[:json].to_s,
      'Accept': 'application/vnd.parking.v1'
    }
  end

  describe 'GET /interactions/:ticket_id' do
    before do
      car.ticket.save
      interaction = build_list(:interaction, 1, ticket_id: car.ticket.id)
      car.interactions << interaction
      interaction.append
      car.ticket.delete
      car.save
      get "/interactions/#{car.ticket.id}", params: {}, headers: headers
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns 1 interaction from data_base' do
      expect(json_body.count).to eq(1)
    end
  end
end
