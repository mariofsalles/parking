require 'rails_helper'

RSpec.describe 'Tickets API', type: :request do
  before { host! 'api.parafuzo.test' }
  let!(:car) { create(:car) }
  let(:headers) do
    {
      'Content-Type': Mime[:json].to_s,
      'Accept': 'application/vnd.parking.v1'
    }
  end

  describe 'GET /tickets' do
    before do
      get '/tickets', params: {}, headers: headers
    end
    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
    it 'return cars list with one ticket embedded' do
      expect(car.ticket.to_a.length).to eq(1)
    end
  end

  describe 'GET /tickets/:id' do
    before { get "/tickets/#{car.ticket.id}", params: {}, headers: headers }
    context 'when I call ticket API with valid code reservation' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
      it 'return json with ticket reservation' do
        expect(json_body['_id']).to eq(car.ticket.id.bson_ruby_as_json)
      end
    end
    context 'when I call ticket API with invalid code reservation' do
      it 'returns status code 404' do
        get '/tickets/invalidCode', params: {}, headers: headers
        expect(response).to have_http_status(404)
      end
      it 'returns error request' do
        get '/tickets/invalidCode', params: {}, headers: headers
        expect(json_body['error_request']).to eq('code not found')
      end
    end
  end
  
end
