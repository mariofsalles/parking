class Api::V1::InteractionsController < ApplicationController
  def show
    @interactions = []
    @interaction = ''
    @code = ''
    Car.each.to_a.each do |c|
      @interactions << c.interactions if c.ticket._id.to_s == params[:id]
      @interactions.each do |i|
        @interaction = i
        @code = :ok
      end
    end
    unless @interaction
      @interaction = { error: 'code not found' }
      @code = :not_found
    end
    render json: { interactions: @interaction }, status: @code
  end
end
