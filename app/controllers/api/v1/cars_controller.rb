class Api::V1::CarsController < ApplicationController
  before_action :load_cars, only: %i[index show pay out]

  # GET /cars
  def index
    render json: @cars, status: :ok
  end

  # GET /cars/:plate
  def show
    interactions = []
    @car = []
    car = ''
    @cars.each do |c|
      interactions << c.ticket if !c.interactions? && (c.plate.to_s == params[:id].upcase)
      interactions << c.interactions if c.interactions? && (c.plate.to_s == params[:id].upcase)
      interactions.each { |i| car = i.to_a }
    end

    unless car.nil?
      car.map do |f|
        @car << { id: f[:_id].to_s, time: f[:time], paid: f[:paid], left: f[:left] }
      end
      @code = :ok
    end

    if car.empty? && params[:id].match(/\A[A-Za-z]{3}-\d{4}\Z/)
      @car = { error_request: 'not registered' }
      @code = :not_found
    end

    if car.empty? && !params[:id].match(/\A[A-Za-z]{3}-\d{4}\Z/)
      @car = { error_format_plate: 'invalid format to the plate property' }
      @code = :bad_request
    end

    render json: @car, status: @code
  end

  # POST /cars
  def create
    ticket = Ticket.new
    car_params[:plate]&.upcase! unless car_params[:plate].nil?
    @car = Car.create(car_params)
    @car.ticket = ticket
    ticket.save

    if @car.persisted?
      car_json = { reservation_number: @car.ticket.id.to_s }
      code = :ok
    end

    if @car.new_record? && !@car.ticket.deleted_at.nil?
      car_json = { error_duplicated_reserved: 'parking already reserved' }
      code = :conflict
    end

    if @car.ticket.deleted_at.nil? || @car.ticket.nil?
      @car = Car.find_by(plate: car_params[:plate])
      @car.update(car_params)
      @car.ticket = ticket
      ticket.save
      car_json = { reservation_number: @car.ticket.id.to_s }
      code = :ok
    end

    if invalid_format_plate?
      car_json = { error_format_plate: 'invalid format to the plate property' }
      code = :bad_request
    end

    if nil_value_plate?
      car_json = { error_fill_out: 'the plate property can not be empty' }
      code = :bad_request
    end

    render json: car_json, status: code
  end

  # PUT /cars/:plate/pay
  def pay
    car = ''
    @cars.each do |c|
      car = c if c.plate.to_s == params[:plate].upcase
    end
    puts car.to_json

    unless car.blank? || car.ticket.paid.nil?
      car.ticket.paid = true
      car.ticket.time = "#{TimeDifference.between(car.ticket.created_at, Time.new).in_minutes} minutes"
      car.ticket.save
      @car = { msg: 'payment succeeded' }
      @code = :ok
    end

    if car.blank? || car.ticket.paid.nil?
      @car = { msg: 'error to find your reservation' }
      @code = :not_found
    end

    render json: @car, status: @code
  end

  # PUT /cars/:plate/out
  def out
    car = ''
    @cars.each do |c|
      car = c if c.ticket.paid? && (c.plate.to_s == params[:plate].upcase)
    end

    unless car.blank? || car.ticket.nil?
      car.ticket.left = true
      car.ticket.save
      @car = { msg: 'thanks to the preference' }
      @code = :ok
      interaction = Interaction.new
      interaction.inside = car.ticket.inside
      interaction.left = car.ticket.left
      interaction.paid = car.ticket.paid
      interaction.time = car.ticket.time
      interaction.ticket_id = car.ticket._id
      car.interactions << interaction
      interaction.save
      car.ticket.delete
    end

    if car.blank?
      @car = { msg: 'check the payment' }
      @code = :not_found
    end
    render json: @car, status: @code
  end

  private

  def car_params
    params.require(:car).permit(:name, :model, :year, :plate)
  end

  def load_cars
    @cars = []
    Car.each.to_a.each do |c|
      @cars << c
    end
  end

  def invalid_format_plate?
    !nil_value_plate? && !@car.plate.match(/\A[A-Za-z]{3}-\d{4}\Z/)
  end

  def nil_value_plate?
    @car.plate.nil?
  end
end
