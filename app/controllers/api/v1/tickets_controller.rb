class Api::V1::TicketsController < ApplicationController
  before_action :load_parent, only: %i[show index]
  # GET /tickets
  def index
    render json: { tickets: @tickets }, status: :ok
  end

  # GET /tickets/:id
  def show
    @tickets.each do |t|
      @ticket = t if t._id.to_s == params[:id]
      @code = :ok
    end

    unless @ticket
      @ticket = { error_request: 'code not found' }
      @code = :not_found
    end
    render json: @ticket, status: @code
  end

  private

  def load_parent
    @tickets = []
    Car.each.to_a.each do |c|
      @tickets << c.ticket
    end
  end
end
