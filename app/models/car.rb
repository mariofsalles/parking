class Car
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  embeds_one :ticket
  embeds_many :interactions

  field :name, type: String
  field :model, type: String
  field :year, type: String
  field :plate, type: String
  index({ plate: 'text' }, { unique: true })
  validates :plate, presence: true,
                    uniqueness: { case_sensitive: false },
                    format: { with: /\A[A-Za-z]{3}-\d{4}\Z/ }
end
