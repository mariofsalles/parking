class Ticket
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  embedded_in :car, class_name: 'Car', inverse_of: :ticket

  field :inside, default: true, type: Mongoid::Boolean
  field :left, default: false, type: Mongoid::Boolean
  field :paid, default: false, type: Mongoid::Boolean
  field :time, type: String
end
