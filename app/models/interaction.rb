class Interaction
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  embedded_in :car, class_name: 'Car', inverse_of: :interactions

  field :inside, type: Mongoid::Boolean
  field :left, type: Mongoid::Boolean
  field :paid, type: Mongoid::Boolean
  field :time, type: String
  field :ticket_id, type: Ticket
  validates :ticket_id, presence: true, uniqueness: true
end
