# Parafuzo Teste

## _Bootstrap do projeto_

##### Parte I: Esse estágio consisitiu em preparar o set que auxiliará na geração do projeto e contém os seguintes arquivos/conteúdos:

1. **Gemfile:**
```Gemfile
source 'https://rubygems.org'
gem 'rails', '~>5'
```
2. **Gemfile.lock:** Neste estágio este arquivo se encontra vazio e 

3. **Dockerfile:** este script apresenta a imagem base do ruby (v2.5), a instalação do mongo (v5.0), um script relacionado a remoção de processos, designação e permissões do projeto

```Dockerfile
# syntax=docker/dockerfile:1
FROM ruby:2.5
RUN apt-get install gnupg
RUN wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | apt-key add -
RUN echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu \\
bionic/mongodb-org/5.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-5.0.list
RUN apt-get update -qq && apt-get install -y nodejs mongodb-org
WORKDIR /parking
COPY Gemfile /parking/Gemfile
COPY Gemfile.lock /parking/Gemfile.lock
RUN bundle install
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
```
4. **docker-compose.yml:** este arquivo `docker-compose.yml` disponibiliza os serviços que estão a ser utlizados neste 1º estágio: para iniciar a aplicação, para iniciar o mongo e possibilidade de consultas a base de dados. Também estão presentes dependencias entre serviços, algumas variáveis de ambiente, volumes e exposições de portas.

```ruby
version: "3.9"
services:
  mongo-express:
    image: mongo-express
    restart: always
    ports:
      - "8081:8081"
    environment:
      ME_CONFIG_MONGODB_URL: mongodb://nosqldb:27017/
    depends_on:
      - nosqldb
  nosqldb:
    image: mongo
    restart: always
    volumes:
      - ./tmp/db:/data/db/
  parking:
    build: .
    command: bash -c "rm -f tmp/pids/server.pid && bundle exec rails s -p 3000 -b '0.0.0.0'"
    volumes:
      - .:/parking
    ports:
      - "3000:3000"
    depends_on:
      - nosqldb
```

5. **.dockerignore**

~~~
Dockerfile
docker-compose.yml
~~~

6. **entrypoint**: o objetivo deste script é remover processos utilizado como entrypoint do arquivo `Dockerfile`

~~~sh
#!/bin/bash
set -e
rm -f /parking/tmp/pids/server.pid
exec "\$@"
~~~

##### Parte II: Execução de scripts e inicialização do projeto:

1. **build:** o build foi realizado pelo comando `docker-compose` a seguir: 

~~~sh
docker-compose run --no-deps parking rails new . --api  -T --force --skip-active-record
~~~
Dessa forma os arquivos são mapeados para o diretório apresentado no volume exposto no arquivo `docker-compose.yaml`, a principio não será gerado o setup de realização dos testes unitários (neste estágio), será puramente uma API e a base de dados não será gerenciada pelo `ActiveRecords`.
Consequentemente um projeto ruby-on-rails é inicializado com o *Gemfile* apresentando as gems para inicio do projeto.

2. **mongoid e testes:** inclui-se as gems para utilização do banco de dados não relacional, realização de teste e o bundle para atualização das bibliotecas com o comando: `docker compose build` seguido, para efeito de ativação dos serviços, o comando: `docker-compose up`.
As gems adicionas para o proposito de criação de testes e interação com a base de dados foram:
2.1. para a base de dados não relacional *mongodb*: `gem 'mongoid'`
2.2. para os *testes* adicionou-se seguintes `gems` entre os grupos `development` e `test`:

```
  gem 'database_cleaner-mongoid'
  gem 'database_cleaner'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'factory_bot'
  gem 'faker'
  gem 'rspec-rails', '~> 3.5'
  gem 'mongoid-rspec
```

3. **configuração da base de dados:** a configuração da base dados é realizada com a execução do comando: 
`docker-compose exec parking rails g mongoid:config`.

4. **bootstrap dos testes (mais uma pincelada):** a partida para realização de testes unitários (ainda sem mencionar as configurações que deverão ser realizadas no diretorio `rspec`) é iniciado pelo comando:
`docker-compose exec parking rails g rspec:install`

##### Parte III: Desenvolvimento do projeto

1. **Gerenciamento do repositório:**
O desafio foi desenvolvido sobre as branchs a seguir:
```
adding-car
adding-interactions
adding-ticket
api-namespaces
```
A denominada **api-namespace** tratou de configurações de versionamento da API e subdominios caso houvesse necessidade.
As demais foram onde ocorreu o desenvolvimento da aplicação

2. **Model, Controller e Testes Unitários:**
Em sua grande maioria o desafio foi elaborado seguindo o fluxo TDD, ou seja tomando como exemplo um trecho de desenvolvimento do dominio **ticket** obtemos:
- Elaboração dos testes relacionados ao **model ticket** em `ticket_spec` com a inclusão de que um objeto ticket deverá ser filho unico de um objeto car.
- Execução do teste pelo comando (o teste irá falhar em todos os steps pois não existe o modelo)
`docker-compose exec parkingt rspec spec/models/ticket_spec.rb`
- Inclusão do model pelo comando scaffold pelo docker-compose:
`docker-compose exec parkingt rails g scaffold ticket inside:boolean left:boolean paid:boolean`
- Reexecução do comando de teste, que irá falhar nos steps onde se verifica a parentalidade entre os objetos **ticket** e **car**, para resolver essa questão no `model ticket` são incluidas as soluções:
`embedded_in :car, class_name: 'Car', inverse_of: :ticket`
- O teste do `objeto ticket` aninhado ao `objeto car`, desencadeia asserts no `model car`, portanto o **car_spec** deve incluído inicialmente para verificação de dependências

O mesmo raciocinio é aplicado para elaboração dos controllers ressaltando que, neste caso testa-se as rotas, actions, response body, status code e alguns métodos.

3. **Postman:**
Uma documentação suplementar com exemplos da aplicação pode ser observada em [Parking API][postman]


[postman]: <https://documenter.getpostman.com/view/5023676/UzBmLmdi>